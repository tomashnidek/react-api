### `yarn install`
### `yarn start`

Changes which needs to be done in java:

Extend JobController with
```  
  @CrossOrigin(origins = "http://localhost:3000")
  @ApiOperation(value = "Returns all jobs", response = JobDTO.class, responseContainer = "List")
  @GetMapping("")
  public List<JobDTO> getAllJobs() {
```
Extend ApplicantController
```  
  @CrossOrigin(origins = "http://localhost:3000") //just add CrossOrigin
  public ApplicantDTO getApplicant()

...

  @CrossOrigin(origins = "http://localhost:3000")
  @ApiOperation(value = "Returns all applicants", response = ApplicantDTO.class, responseContainer = "List")
  @GetMapping("") // Annotation for mapping HTTP GET requests onto specific handler methods.
  public List<ApplicantDTO> getAllApplicants()

...

  @CrossOrigin(origins = "http://localhost:3000")
  @ApiOperation(value = "Changes an applicant")
  @PutMapping
  public void updateApplicant(@RequestBody ApplicantDTO applicant)
```
and add `@RequestBody` to parameter in `createApplicant`
Extend ApplicantDTO
```
private Integer id;
```

and provide implementation to all of this. Keep in mind crossorigin is important. This uses localhost:8102 for job service access and localhost:8100 for applicant service.

Design is horrible. Not the point. :)
