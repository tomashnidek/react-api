import React, {Component} from 'react'

    class Address extends Component {
      constructor(props) {
        super(props);
        this.renderAddressRow = this.renderAddressRow.bind(this);
      }
      renderAddressRow(prop) {
          if(!prop) {
            return <div></div>;
          } else {
            return (<div>{prop}<br /></div>);
          }
        }

      render() {
        const addr = this.props.addr;
      if (!addr) return (<div></div>)
      return (
        <address>
          {this.renderAddressRow(addr.address1)}
          {this.renderAddressRow(addr.address2)}
          {this.renderAddressRow(addr.city)}
          {this.renderAddressRow(addr.state)}
          {this.renderAddressRow(addr.country)}
          {this.renderAddressRow(addr.postalCode)}
        </address>
      )
    };
  }

    export default Address
