import React, {Component} from 'react'
import {Form, Label, FormGroup, Input, Button, Container, Row, Col} from 'reactstrap'

    class ApplicantCreate extends Component {
      constructor(props) {
        super(props);
        this.state = {
          firstName: '',
          lastName: '',
          email: '',
          nationalId: '',
          citizenship: '',
          address1: '',
          address2: '',
          country: '',
          state: '',
          city: '',
          postalCode: ''
        }
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.createObject = this.createObject.bind(this);
      }

      createObject() {
        return {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          email: this.state.email,
          nationalId: this.state.nationalId,
          citizenship: this.state.citizenship,
          address: {
            address1: this.state.address1,
            address2: this.state.address2,
            country: this.state.country,
            state: this.state.state,
            city: this.state.city,
            postalCode: this.state.postalCode
          }
        }
      }

      onFormSubmit(e) {
        e.preventDefault();
        fetch('http://localhost:8100/api/applicants', {
          method: 'POST',

          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify( this.createObject())
        })
        .then(res => {
          if(res.status === 200) {
            this.props.history.push('/applicants')
          }
        })
        .catch(res => console.log(res));
      }

      render() {
      return (
        <div>
        <Container>
          <Row>
            <Col md={{ size: 6, offset: 3 }}>
            <Form onSubmit={this.onFormSubmit}>
              <FormGroup tag="fieldset">
                <legend>User</legend>
                <FormGroup>
                  <Label for="firstName">First Name</Label>
                  <Input type="text" name="firstName" id="firstName" onChange={e => this.setState({ firstName: e.target.value })} />
                </FormGroup>
                <FormGroup>
                  <Label for="lastName">Last Name</Label>
                  <Input type="text" name="lastName" id="lastName" onChange={e => this.setState({ lastName: e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input type="text" name="email" id="email" onChange={e => this.setState({ email: e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="nationalId">National Id</Label>
                  <Input type="text" name="nationalId" id="nationalId" onChange={e => this.setState({ nationalId: e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="citizenship">Citizenship</Label>
                  <Input type="text" name="citizenship" id="citizenship" onChange={e => this.setState({ citizenship: e.target.value })} />
                </FormGroup>
              </FormGroup>
              <FormGroup tag="fieldset">
                <legend>Address</legend>
                <FormGroup>
                  <Label for="address1">Primary</Label>
                  <Input type="text" name="address1" id="address1" onChange={e => this.setState({ 'address1': e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="address2">Secondary</Label>
                  <Input type="text" name="address2" id="address2" onChange={e => this.setState({ 'address2': e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="country">Country</Label>
                  <Input type="text" name="country" id="country" onChange={e => this.setState({ 'country': e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="state">State</Label>
                  <Input type="text" name="state" id="state" onChange={e => this.setState({ 'state': e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="city">City</Label>
                  <Input type="text" name="city" id="city" onChange={e => this.setState({ 'city': e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="postalCode">Postal Code</Label>
                  <Input type="text" name="postalCode" id="postalCode" onChange={e => this.setState({ 'postalCode': e.target.value })}/>
                </FormGroup>
              </FormGroup>
              <Button>Submit</Button>

            </Form>
            </Col>
            </Row>
          </Container>
        </div>
      )
    };
  }

    export default ApplicantCreate
