import React from 'react'
import Address from './Address'
import {Card, CardBody, CardTitle, CardText} from 'reactstrap'

    const ApplicantSelected = ({ applicant }) => {
      if(!applicant) return (<div></div>);
      return (
        <div>
            <Card>
              <CardBody>
                <CardTitle>{applicant.firstName} {applicant.lastName}</CardTitle>
                <CardText tag="div">
                  <dl>
                    <dt>Email</dt>
                    <dd>{applicant.email}</dd>
                    <dt>National Id</dt>
                    <dd>{applicant.nationalId}</dd>
                    <dt>Citizenship</dt>
                    <dd>{applicant.citizenship}</dd>
                    <dt>Address</dt>
                    <dd><Address addr={applicant.address} /></dd>
                  </dl>
                </CardText>
              </CardBody>
            </Card>
        </div>
      )
    };

    export default ApplicantSelected
