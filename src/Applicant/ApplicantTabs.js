import React from 'react'
import {Card, CardBody, CardTitle, CardSubtitle} from 'reactstrap'

    const ApplicantTabs = ({ applicants, applicantSelectedCallback }) => {
      return (
        <div>

          {applicants.map((applicant) => (
            <Card tag="a" key={applicant.id} id={applicant.id}  style={{ cursor: 'pointer' }} onClick={applicantSelectedCallback}>
              <CardBody>
                <CardTitle>{applicant.firstName} {applicant.lastName}</CardTitle>
                <CardSubtitle>{applicant.citizenship}</CardSubtitle>
              </CardBody>
            </Card>
          ))}
        </div>
      )
    };

    export default ApplicantTabs
