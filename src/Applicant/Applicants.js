import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import '../App.css';
import ApplicantTabs from './ApplicantTabs';
import ApplicantSelected from './ApplicantSelected';
import {Container, Row, Col, Button} from 'reactstrap'

 class Applicants extends Component {
   constructor(props) {
     super(props);
     // Don't call this.setState() here!
     this.state = {
       applicants: [],
       selectedApplicant: {},
     };
     this.applicantSelectedCallback = this.applicantSelectedCallback.bind(this);
   }

  applicantSelectedCallback(e) {
    const id = e.currentTarget.id;
    for (var applicant of this.state.applicants) {
      if (applicant.id.toString() === id) {
        this.setState({selectedApplicant: applicant});
      }
    }
  }

  componentDidMount() {
        fetch('http://localhost:8100/api/applicants')
        .then(res => res.json())
        .then((data) => {
          this.setState({ applicants: data })
          this.setState({ selectedApplicant: data[0] })
        })
        .catch(console.log);
      };
      render() {
  return (
    <div className="App">
    <center><h1>Applicant List</h1></center>
      <Container>
        <Row>
          <Col xs="3"><ApplicantTabs applicants={this.state.applicants} applicantSelectedCallback={this.applicantSelectedCallback}/><br />
          <Link to="/applicant-create"><Button>Add</Button></Link>
          </Col>
          <Col xs="9"><ApplicantSelected applicant={this.state.selectedApplicant} /></Col>
        </Row>
      </Container>
    </div>
  );
}
}

export default Applicants;
