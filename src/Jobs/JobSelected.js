import React from 'react'
import {Card, CardBody, CardTitle, CardText} from 'reactstrap'

    const JobTabs = ({ job }) => {
      return (
        <div>
            <Card>
              <CardBody>
                <CardTitle>{job.positionTitle}</CardTitle>
                <CardText>{job.positionDescription}</CardText>
              </CardBody>
            </Card>
        </div>
      )
    };

    export default JobTabs
