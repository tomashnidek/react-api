import React from 'react'
import {Card, CardBody, CardTitle, CardSubtitle} from 'reactstrap'

    const JobTabs = ({ jobs, jobSelectedCallback }) => {
      return (
        <div>

          {jobs.map((job) => (
            <Card tag="a" key={job.id} id={job.id}  style={{ cursor: 'pointer' }} onClick={jobSelectedCallback}>
              <CardBody>
                <CardTitle>{job.positionTitle}</CardTitle>
                <CardSubtitle>Id: {job.id}</CardSubtitle>
              </CardBody>
            </Card>
          ))}
        </div>
      )
    };

    export default JobTabs
