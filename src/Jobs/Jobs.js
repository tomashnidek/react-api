import React, {Component} from 'react';
import '../App.css';
import JobTabs from './JobTabs';
import JobSelected from './JobSelected';
import {Container, Row, Col} from 'reactstrap'

 class Jobs extends Component {
   constructor(props) {
     super(props);
     // Don't call this.setState() here!
     this.state = {
       jobs: [],
       selectedJob: {},
     };
     this.jobSelectedCallback = this.jobSelectedCallback.bind(this);
   }

  jobSelectedCallback(e) {
    const id = e.currentTarget.id;
    for (var job of this.state.jobs) {
      if (job.id.toString() === id) {
        this.setState({selectedJob: job});
      }
    }
  }

  componentDidMount() {
        fetch('http://localhost:8102/api/jobs')
        .then(res => res.json())
        .then((data) => {
          this.setState({ jobs: data })
          this.setState({selectedJob: data[0]})
        })
        .catch(console.log);
      };
      render() {
  return (
    <div className="App">
    <center><h1>Job List</h1></center>
      <Container>
        <Row>
          <Col xs="3"><JobTabs jobs={this.state.jobs} jobSelectedCallback={this.jobSelectedCallback}/></Col>
          <Col xs="9"><JobSelected job={this.state.selectedJob}></JobSelected></Col>
        </Row>
      </Container>
    </div>
  );
}
}

export default Jobs;
