import React, {Component} from 'react'
import {BrowserRouter} from 'react-router-dom';
import {Route, Switch} from 'react-router';
import Jobs from './Jobs/Jobs';
import TopBar from './TopBar';
import Profile from './Profile/Profile';
import ProfileEdit from './Profile/ProfileEdit';
import Applicants from './Applicant/Applicants';
import ApplicantCreate from './Applicant/ApplicantCreate'
import Login from './Login';

 class Main extends Component {
      render() {
  return (
    <BrowserRouter>
    <div>
    <TopBar />
    <Switch>
      <Route exact={true} path="/" component={Jobs} />
      <Route path="/applicants" component={Applicants} />
      <Route path="/applicant-create" component={ApplicantCreate} />
      <Route path="/profile/:id?" component={Profile} />
      <Route path="/profile-edit/:id" component={ProfileEdit} />
      <Route path="/login" component={Login} />
      </Switch>
    </div>
  </BrowserRouter>
  );
}
}

export default Main;
