import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import {Container, Row, Col, Button} from 'reactstrap';

import ProfileSelector from './ProfileSelector';
import ProfileSelected from './ProfileSelected';

    class Profile extends Component {
      constructor(props) {
        super(props);
        this.actionOnSelect = this.actionOnSelect.bind(this);
        this.state = {
          selected: 0,
          applicants: [],
          selectedApplicant: []
        }
      }

      actionOnSelect(e) {
        const id = e.currentTarget.value;
        fetch('http://localhost:8100/api/applicants/'+id)
        .then(res => res.json())
        .then((data) => {
          this.setState({ selectedApplicant: data, selected: id })
        })
        .catch(console.log);
      }

      componentDidMount() {
        const { match: { params } } = this.props;
            fetch('http://localhost:8100/api/applicants')
            .then(res => res.json())
            .then((data) => {
              const reducedApplicants = [];
              for(let applicant of data) {
                reducedApplicants.push({name: applicant.firstName +' ' +applicant.lastName, id: applicant.id});
              }
              this.setState({ applicants: reducedApplicants });
              const select = (params.id -1) || 0;
              this.setState({ selectedApplicant: data[select] });
              this.setState({ selected: params.id || 0 });
            })
            .catch(console.log);
          };

      render() {
        const buttonLink = '/profile-edit/'+this.state.selectedApplicant.id;
      return (
        <div>
          <center><h1>Profile</h1></center>
          <Container>
            <Row>
            <Col xs="12">
          <ProfileSelector selected={this.state.selected} applicants={this.state.applicants} actionOnSelect={this.actionOnSelect}/>
          </Col>

          </Row>
          <Row>
          <ProfileSelected selectedApplicant={this.state.selectedApplicant}/>
          </Row>
          <Row>
          <Link to={buttonLink}><Button>Edit</Button></Link>
          </Row>

          </Container>
        </div>
      )
    };
  }

    export default Profile
