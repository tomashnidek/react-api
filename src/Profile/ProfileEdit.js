import React, {Component} from 'react';
import {Form, Label, FormGroup, Input, Button, Container, Row, Col} from 'reactstrap'

    class ProfileEdit extends Component {
      constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.state = {
          applicant: {},
          email: '',
          nationalId: '',
          citizenship: ''
        }
      }

      createObject() {
        return {
          id: this.state.applicant.id,
          firstName: this.state.applicant.firstName,
          lastName: this.state.applicant.lastName,
          email: this.state.email,
          nationalId: this.state.nationalId,
          citizenship: this.state.citizenship,
          address: {
            address1: this.state.applicant.address.address1,
            address2: this.state.applicant.address.address2,
            country: this.state.applicant.address.country,
            state: this.state.applicant.address.state,
            city: this.state.applicant.address.city,
            postalCode: this.state.applicant.address.postalCode
          }
        }
      }

      onFormSubmit(e) {
        e.preventDefault();
        fetch('http://localhost:8100/api/applicants', {
          method: 'PUT',

          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify( this.createObject())
        })
        .then(res => {
          if(res.status === 200) {
            this.props.history.push('/profile/'+this.state.applicant.id)
          }
        })
        .catch(res => console.log(res));
      }

      componentDidMount() {
        const { match: { params } } = this.props;
        const id = params.id;
        fetch('http://localhost:8100/api/applicants/'+id)
        .then(res => res.json())
        .then((data) => {
          this.setState({ applicant: data,
          email: data.email,
          nationalId: data.nationalId,
          citizenship: data.citizenship })
        })
        .catch(console.log);
      }

      render() {
        const { applicant } = this.state;
        if(!applicant) return (<div></div>);
      return (
        <div>
        <Container>
          <Row>
            <Col md={{ size: 6, offset: 3 }}>
            <Form onSubmit={this.onFormSubmit}>
              <FormGroup tag="fieldset">
                <legend>User</legend>
                <p>{applicant.firstName} {applicant.lastName}</p>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input type="text" value={this.state.email} name="email" id="email" onChange={e => this.setState({ email: e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="nationalId">National Id</Label>
                  <Input type="text" value={this.state.nationalId} name="nationalId" id="nationalId" onChange={e => this.setState({ nationalId: e.target.value })}/>
                </FormGroup>
                <FormGroup>
                  <Label for="citizenship">Citizenship</Label>
                  <Input type="text" value={this.state.citizenship} name="citizenship" id="citizenship" onChange={e => this.setState({ citizenship: e.target.value })} />
                </FormGroup>
              </FormGroup>
              <p>Omitting address edit</p>
              <Button>Submit</Button>

            </Form>
            </Col>
            </Row>
          </Container>
        </div>
      )
    };
  }

    export default ProfileEdit
