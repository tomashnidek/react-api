import React from 'react';
import Address from '../Applicant/Address';

    const ProfileSelected = ({ selectedApplicant }) => {
      return (
        <div>
        <dl>
          <dt>Email</dt>
          <dd>{selectedApplicant.email}</dd>
          <dt>National Id</dt>
          <dd>{selectedApplicant.nationalId}</dd>
          <dt>Citizenship</dt>
          <dd>{selectedApplicant.citizenship}</dd>
          <dt>Address</dt>
          <dd><Address addr={selectedApplicant.address} /></dd>
        </dl>
        </div>
      )
    };

    export default ProfileSelected
