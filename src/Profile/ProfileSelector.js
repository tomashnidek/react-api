import React from 'react'
import {FormGroup, Label, Input} from 'reactstrap'

    const ProfileSelector = ({ selected, applicants, actionOnSelect }) => {
      return (
        <div>
        <FormGroup>
          <Label for="exampleSelect">Select</Label>
          <Input type="select" name="select" id="exampleSelect" value={selected} onChange={actionOnSelect}>
            {applicants.map((applicant) => (
              <option id={applicant.id} key={applicant.id} value={applicant.id}>{applicant.name}</option>
              ))}
          </Input>
        </FormGroup>
        </div>
      )
    };

    export default ProfileSelector
