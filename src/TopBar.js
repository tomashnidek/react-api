import React from 'react'
import {Navbar, NavbarBrand, NavItem, Nav, NavLink} from 'reactstrap'

    const TopBar = () => {
      return (
        <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">jobs</NavbarBrand>
          <NavbarBrand href="/applicants">applicants</NavbarBrand>
          <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink href="/profile">profile</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/login">login</NavLink>
            </NavItem>
            </Nav>
        </Navbar>
        </div>
      )
    };

    export default TopBar
